package ru.t1.volkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Role;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Role role = Role.USUAL;

    @NotNull
    private Boolean locked = false;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(login, user.login) &&
                Objects.equals(email, user.email) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(middleName, user.middleName) &&
                role.getDisplayName().equals(user.role.getDisplayName()) &&
                locked.equals(user.locked);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, email, firstName, lastName, middleName, role, locked);
    }
}
