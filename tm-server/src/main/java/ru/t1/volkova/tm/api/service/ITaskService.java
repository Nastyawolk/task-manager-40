package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.model.Task;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public interface ITaskService {

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws SQLException;

    @NotNull
    Task updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    ) throws SQLException;

    @NotNull
    Task changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @NotNull Status status
    ) throws SQLException;

    @Nullable
    Task changeTaskStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @NotNull Status status
    ) throws SQLException;

    @Nullable
    List<Task> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    ) throws SQLException;

    @Nullable
    List<Task> findAlls() throws SQLException;

    @NotNull
    Task create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws SQLException;

    @NotNull
    Task add(@NotNull Task task) throws SQLException;

    @Nullable
    List<Task> findAll(@Nullable String userId) throws SQLException;

    @Nullable
    List<Task> findAll(@Nullable String userId, @Nullable Comparator comparator) throws Exception;

    @Nullable
    List<Task> findAllByName(@Nullable String userId) throws SQLException;

    @Nullable
    List<Task> findAllByStatus(@Nullable String userId) throws SQLException;

    @Nullable
    List<Task> findAllByCreated(@Nullable String userId) throws SQLException;

    @NotNull
    Task findOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @NotNull
    Task findOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    int getSize(@Nullable String userId) throws SQLException;

    @Nullable
    Task removeOneById(@Nullable String userId, @Nullable String id) throws SQLException;

    @Nullable
    Task removeOneByIndex(@Nullable String userId, @NotNull Integer index) throws SQLException;

    @Nullable
    Task removeOne(@Nullable String userId, @NotNull Task task) throws SQLException;

    void removeAll(@Nullable String userId) throws SQLException;

    void update(@NotNull Task task) throws SQLException;
}
