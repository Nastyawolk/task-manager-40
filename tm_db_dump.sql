--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

-- Started on 2022-09-29 23:30:13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 209 (class 1259 OID 16425)
-- Name: tm_project; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.tm_project (
    id text NOT NULL,
    name text,
    created date,
    description text,
    user_id text,
    status text
);


ALTER TABLE public.tm_project OWNER TO admin;

--
-- TOC entry 212 (class 1259 OID 16446)
-- Name: tm_session; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.tm_session (
    id text NOT NULL,
    date date,
    user_id text,
    role text
);


ALTER TABLE public.tm_session OWNER TO admin;

--
-- TOC entry 210 (class 1259 OID 16430)
-- Name: tm_task; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.tm_task (
    id text NOT NULL,
    name text,
    created date,
    description text,
    user_id text,
    status text,
    project_id text
);


ALTER TABLE public.tm_task OWNER TO admin;

--
-- TOC entry 211 (class 1259 OID 16435)
-- Name: tm_user; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.tm_user (
    id text NOT NULL,
    login text,
    password text,
    email text,
    locked boolean,
    first_name text,
    last_name text,
    middle_name character varying,
    role text
);


ALTER TABLE public.tm_user OWNER TO admin;

--
-- TOC entry 3322 (class 0 OID 16425)
-- Dependencies: 209
-- Data for Name: tm_project; Type: TABLE DATA; Schema: public; Owner: admin
--

INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('0537dbbf-b832-4bcc-a37d-ced7db363d81', 'new_project', '2022-09-26', 'new description', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'NOT_STARTED');
INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('5c26297e-5baa-4b20-b05b-f9d276df59c3', 'new project', '2022-09-07', 'new project', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'NOT_STARTED');
INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('8d7d4ba5-16fe-45d6-a6ef-4ffaf67833eb', 'new project', '2022-09-26', 'new project', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'IN_PROGRESS');
INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('6bd5308a-0a6f-4525-ad3d-bc6d90f49a4d', 'PROJECT12345', '2022-09-29', 'Project for TestUser', 'df17407d-4764-4bd9-8cad-232f73e3f8b1', 'NOT_STARTED');
INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('c868b203-2d26-476f-b7c7-96f6c173129d', 'PROJECT444', '2022-09-29', 'Project for CustomUser', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'NOT_STARTED');
INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('c8ca6b4d-d11a-4094-bf8a-d0ec8df56753', '', '2022-09-28', '', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'NOT_STARTED');
INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('da877f8c-b293-4f78-83a5-80d5221dd6aa', 'PROJECT12', '2022-09-29', 'Project 2 for TestUser', 'df17407d-4764-4bd9-8cad-232f73e3f8b1', 'NOT_STARTED');
INSERT INTO public.tm_project (id, name, created, description, user_id, status) VALUES ('fa7670dd-3258-4db8-9abc-f6c7c3bb3f57', 'project223', '2022-09-29', 'project2929', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'NOT_STARTED');


--
-- TOC entry 3325 (class 0 OID 16446)
-- Dependencies: 212
-- Data for Name: tm_session; Type: TABLE DATA; Schema: public; Owner: admin
--

INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('d6a19389-240b-4fe1-b4b9-1b787f4a9d88', '2022-09-26', 'df17407d-4764-4bd9-8cad-232f73e3f8b1', 'USUAL');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('c1eaa837-d597-4d0b-87c4-4ba054fc2dc5', '2022-09-26', 'df17407d-4764-4bd9-8cad-232f73e3f8b1', 'USUAL');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('d124c583-43c5-4ece-9913-1328acd02cd0', '2022-09-29', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'USUAL');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('e62bc92d-4040-4f2a-92af-0662751197ac', '2022-09-29', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'USUAL');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('061c890f-2a04-4c13-a82a-5016d608ffd1', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('43ecee95-921c-4a27-a7ff-356560db22d1', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('f8cd6264-2f29-41a0-b024-9db96a3198bc', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('255c0037-3971-4f7b-ab38-9e57963bbb57', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('b754292b-8adf-47ec-b63a-27c0d1a4bf8c', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('b163fd51-99ca-4f6e-9a16-bd7009df9f50', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('204e4aa2-36e2-46f5-9114-f730122e5495', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');
INSERT INTO public.tm_session (id, date, user_id, role) VALUES ('9ade48a4-ebf3-4140-8dd8-854ab128783b', '2022-09-07', '488cc731-15ba-4e30-ac04-c58444d35671', 'Administrator');


--
-- TOC entry 3323 (class 0 OID 16430)
-- Dependencies: 210
-- Data for Name: tm_task; Type: TABLE DATA; Schema: public; Owner: admin
--

INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('8077884f-208b-4648-934c-72d4b159d9f7', 'new_task', '2022-09-29', 'new description', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'IN_PROGRESS', NULL);
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('941f3a0e-e321-47cc-98e3-1d132ec48aee', 'TASK', '2022-09-29', 'test task', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'NOT_STARTED', NULL);
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('d14af891-cf4d-4ca9-828a-31fedee58c14', 'TASK1234', '2022-09-29', 'test task', 'df17407d-4764-4bd9-8cad-232f73e3f8b1', 'NOT_STARTED', NULL);
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('f7a71b48-bc5a-45a2-af09-e2bf2a893acc', 'new nameUpd', '2022-09-29', 'new descUpd', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'NOT_STARTED', NULL);
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('6f57cde1-decc-4797-986e-60324635dbe4', 'new_task', '2022-09-29', 'new description', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'NOT_STARTED', NULL);
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('f9eaeff2-c753-4998-b6b9-54b1f489a667', 'TASK5', '2022-09-29', 'test task', '37438da3-a264-45ae-9dda-29f8fa4817ee', 'NOT_STARTED', NULL);
INSERT INTO public.tm_task (id, name, created, description, user_id, status, project_id) VALUES ('f636fc48-7803-46df-b3a1-ce520585fe1d', 'TASK12', '2022-09-29', 'test task2', 'df17407d-4764-4bd9-8cad-232f73e3f8b1', 'NOT_STARTED', NULL);


--
-- TOC entry 3324 (class 0 OID 16435)
-- Dependencies: 211
-- Data for Name: tm_user; Type: TABLE DATA; Schema: public; Owner: admin
--

INSERT INTO public.tm_user (id, login, password, email, locked, first_name, last_name, middle_name, role) VALUES ('bc89b467-c04d-4806-be26-4e01a053eb42', 'new_login', '0bca328bbb58b8ec24c7f3eaed671d3d', NULL, false, 'new name', 'new lastName', 'new middleName', 'USUAL');
INSERT INTO public.tm_user (id, login, password, email, locked, first_name, last_name, middle_name, role) VALUES ('df17407d-4764-4bd9-8cad-232f73e3f8b1', 'test', 'd969f78121bc32c1e5b889c533185a00', 'test@mail.ru', false, NULL, NULL, NULL, 'USUAL');
INSERT INTO public.tm_user (id, login, password, email, locked, first_name, last_name, middle_name, role) VALUES ('37438da3-a264-45ae-9dda-29f8fa4817ee', 'user', 'e185e7f2ef1f3b8a18abae0f241c4b13', 'user@user.ru', false, NULL, NULL, NULL, 'USUAL');
INSERT INTO public.tm_user (id, login, password, email, locked, first_name, last_name, middle_name, role) VALUES ('488cc731-15ba-4e30-ac04-c58444d35671', 'admin', '677eca73dc6474da8d041eedab5f1af8', NULL, false, NULL, NULL, NULL, 'ADMIN');


--
-- TOC entry 3176 (class 2606 OID 16454)
-- Name: tm_project tm_project_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.tm_project
    ADD CONSTRAINT tm_project_pkey PRIMARY KEY (id);


--
-- TOC entry 3182 (class 2606 OID 16456)
-- Name: tm_session tm_session_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.tm_session
    ADD CONSTRAINT tm_session_pkey PRIMARY KEY (id);


--
-- TOC entry 3178 (class 2606 OID 16458)
-- Name: tm_task tm_task_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.tm_task
    ADD CONSTRAINT tm_task_pkey PRIMARY KEY (id);


--
-- TOC entry 3180 (class 2606 OID 16460)
-- Name: tm_user tm_user_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.tm_user
    ADD CONSTRAINT tm_user_pkey PRIMARY KEY (id);


-- Completed on 2022-09-29 23:30:14

--
-- PostgreSQL database dump complete
--

